// Fill out your copyright notice in the Description page of Project Settings.


#include "InvocatorClientComponent.h"
#include "../Actors/PlatformInvocator.h"
// Sets default values for this component's properties

void UInvocatorClientComponent::OnPlatformMoved()
{
	if (OnPlatformMovedClient.IsBound())
	{
		// GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::White, FString::Printf(TEXT("OnPlatformMoved Client")));
		OnPlatformMovedClient.Broadcast();
	}
}

// Called when the game starts
void UInvocatorClientComponent::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(Invocator))
	{
		Invocator->OnPlatformMoved.AddDynamic(this, &UInvocatorClientComponent::OnPlatformMoved);
	}
}

