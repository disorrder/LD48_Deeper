// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InvocatorClientComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlatformMovedClient);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LD48_DEEPER_API UInvocatorClientComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditInstanceOnly, Category = "Platform Invocator Client Settings")
	class APlatformInvocator* Invocator = nullptr;

	UPROPERTY(BlueprintAssignable)
	FOnPlatformMovedClient OnPlatformMovedClient;

	UFUNCTION(BlueprintCallable)
	void OnPlatformMoved();

	// Called when the game starts
	virtual void BeginPlay() override;



		
};
