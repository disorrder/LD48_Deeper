// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD48_DeeperGameMode.h"
#include "LD48_DeeperCharacter.h"
#include "UObject/ConstructorHelpers.h"

ALD48_DeeperGameMode::ALD48_DeeperGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
