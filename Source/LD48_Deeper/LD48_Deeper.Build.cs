// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class LD48_Deeper : ModuleRules
{
	public LD48_Deeper(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
