#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "BasePlatform.generated.h"

UENUM()
enum class EPlatformBehavior : uint8
{
	OnDemand = 0, //��������� ������������ ������ ��� �������
	Loop //��������� ������������ �� �����.
};

UCLASS()
class LD48_DEEPER_API ABasePlatform : public AActor {

GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABasePlatform();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Platform Settings")
	UStaticMeshComponent* PlatformMeshComponent = nullptr;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, meta = (MakeEditWidget), Category = "Platform Settings")
	FVector EndLocation = FVector::ZeroVector;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Transient, Category = "Platform Settings")
	FVector StartLocation = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Platform Settings")
	EPlatformBehavior PlatformBehavior = EPlatformBehavior::OnDemand;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Platform Settings")
	UCurveFloat* PlatformMovementCurve = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Platform Settings")
	float ForwardMovingRate = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Platform Settings")
	float PlatformMovementTimeout = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Platform Settings")
	float BackwardMovingRate = 0.0f;

	UFUNCTION(BlueprintCallable)
	void StartPlatformMovement();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:

	UFUNCTION()
	void UpdatePlatformMovement(float Value);

	UFUNCTION()
	void MovePlatformOnDemand();

	UFUNCTION()
	void TimelineHasCompleted();

	FTimeline PlatformMovementTimeline;

	FTimerHandle TimerHandle;

	FTimerDelegate TimerDel;
};