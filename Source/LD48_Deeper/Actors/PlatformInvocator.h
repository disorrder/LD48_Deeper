// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlatformInvocator.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlatformMoved);

UCLASS()
class LD48_DEEPER_API APlatformInvocator : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FOnPlatformMoved OnPlatformMoved;

protected:
	UFUNCTION(BlueprintCallable)
	void MovePlatform();
};