#include "BasePlatform.h"
#include "Math/UnrealMathVectorCommon.h"
#include "TimerManager.h"
#include "Components/SceneComponent.h"


ABasePlatform::ABasePlatform()
{

	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* DefaultPlatformRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Platform root"));
	RootComponent = DefaultPlatformRoot;

	PlatformMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform"));
	PlatformMeshComponent->SetupAttachment(DefaultPlatformRoot);
}

void ABasePlatform::MovePlatformOnDemand()
{
	if (PlatformMeshComponent->GetRelativeLocation().Equals(StartLocation))
	{
		PlatformMovementTimeline.SetPlayRate(ForwardMovingRate);
		PlatformMovementTimeline.Play();
	}
	else {
		PlatformMovementTimeline.SetPlayRate(BackwardMovingRate);
		PlatformMovementTimeline.Reverse();
	}
}

void ABasePlatform::StartPlatformMovement()
{
	TimelineHasCompleted();
}

void ABasePlatform::BeginPlay()
{
	Super::BeginPlay();
	StartLocation = PlatformMeshComponent->GetRelativeLocation();

	if (IsValid(PlatformMovementCurve))
	{
		FOnTimelineFloat TimelineCallback;
		FOnTimelineEventStatic EndTimelineEvent;

		TimelineCallback.BindUFunction(this, FName("UpdatePlatformMovement"));
		PlatformMovementTimeline.AddInterpFloat(PlatformMovementCurve, TimelineCallback);

		if (PlatformBehavior == EPlatformBehavior::Loop)
		{
			EndTimelineEvent.BindUFunction(this, FName("TimelineHasCompleted"));
			PlatformMovementTimeline.SetTimelineFinishedFunc(EndTimelineEvent);

			// Initial Platform movement on Begin Play
			TimerDel.BindUFunction(this, FName("MovePlatformOnDemand"));
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::White, FString::Printf(TEXT("Timer from Begin Play")));
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, PlatformMovementTimeout, false);
		}
	}
}

void ABasePlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	PlatformMovementTimeline.TickTimeline(DeltaTime);
}

void ABasePlatform::UpdatePlatformMovement(float Value)
{
	FVector NewLocation = FMath::Lerp(StartLocation, EndLocation, Value);
	PlatformMeshComponent->SetRelativeLocation(NewLocation);
}

void ABasePlatform::TimelineHasCompleted()
{
	TimerDel.BindUFunction(this, FName("MovePlatformOnDemand"));
	GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::White, FString::Printf(TEXT("Timer from TimelineHasCompleted")));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, PlatformMovementTimeout, false);
}
